<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include 'header.inc';
include "tools.inc";


$id = getIDFromParams("ResearchArea");

$result = mysql_query("SELECT Name from ResearchArea WHERE uid=".$id.";");
$row = mysql_fetch_assoc($result);
$area_name = $row['Name'];

SPITHEADER($area_name,$MAGIC_MASONRY_STUFF);

?>

<body>
<div id="area-page" class="page-container">

<div id="title">
  <a href="index"><img src="pics/title.png" style="width:100%" alt="Flux Research Group / School of Computing"></a>
</div>

<?php SPITMENU("people"); ?>

<div class="box-shadow people-listing">

<p class="section-title">
<?php echo strtolower($area_name)?> people
</p>

<?php 
    peopleList(array("areaID" => $id, "includeAffil" => 1));  
?>
  
<div style="clear:both;"> </div>
</div>

<?php
$result = mysql_query("SELECT ProjectID from RelProjectArea WHERE AID='$id'");
if (mysql_num_rows($result) > 0) {
?>

<div class="box-shadow projects-listing">
<p class="section-title"><?php echo strtolower($area_name)?> projects</p>
<div id="masonry-container">
<?php
    while($row = mysql_fetch_assoc($result)) {
        showProjectProfile($row['ProjectID']);
    }
?>
</div>
</div>
<div style="clear:both;"> </div>

<?php
}
?>


<div class="box-shadow papers-listing">
<p class="section-title">
recent <?php echo strtolower($area_name)?> publications 
<span class="subtitle"><a href="pubs/<?php echo tag("ResearchArea",$id)?>">(see all)</a></span>
</p> 

<?php
$text = getPublicationsModule(array("areaID" => $id, "limit" => 5));
echo $text;
?>
</div>

</div>

</body>
</html>


