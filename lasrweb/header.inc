<?php

# See config.inc.example to buid one of these
include 'config.inc';

#
# Some common junk used on many pages
#

$MAGIC_MASONRY_STUFF =
    "<script type=\"text/javascript\" src=\"jquery/jquery-1.10.2.min.js\"></script> " .
    "<script type=\"text/javascript\" src=\"jquery/masonry.pkgd.min.js\"></script>" .
    "<script type=\"text/javascript\" src=\"jquery/imagesloaded.pkgd.min.js\"></script>" .
    "<script type=\"text/javascript\"> " .
    "$(document).ready( function() { var \$container = \$('#masonry-container'); ".
    "\$container.imagesLoaded( function() { \$container.masonry({itemSelector:'.project-profile'}); }); });" .
    "</script>";

#
# Functions
#

$db = mysql_connect($DATABASE_HOST,$DATABASE_USERNAME,$DATABASE_PASSWORD);
mysql_select_db($DATABASE_NAME, $db);

function SPITHEADER($title,$extra = "",$is_frontpage = FALSE){
    global $PAGETITLE;
    global $PAGETITLE;
    global $EXTRA_HEADERS;
    global $BASE;

    # Check to see if we were served with SSL
    if (empty($_SERVER['HTTPS'])) {
        $url = "http://" . $BASE;
    } else {
        $url = "https://" . $BASE;
    }

    # Find out our file name so that we can include a special CSS
    $pagename = preg_replace('/\.php$/', '', $_SERVER['SCRIPT_NAME']);

?>
<html>
<head>
<?php
    if (isset($BASE)) {
        echo "    <base href=\"$url\">\n";
    }
    if ($is_frontpage) {
        $fulltitle = $title;
    } else if (!empty($title)) {
        $fulltitle = "$title : $PAGETITLE";
    } else {
        $fulltitle = $PAGETITLE;
    }
?>
    <title><?php echo "$fulltitle"; ?></title>
    <link href="global.css" rel="stylesheet" type="text/css">
    <link href="listing.css" rel="stylesheet" type="text/css">
    <link href="transparency.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $pagename?>.css" rel="stylesheet" type="text/css">
    <?php echo "$extra" ?>
    <?php if (!empty($EXTRA_HEADERS)) { echo $EXTRA_HEADERS; } ?>
</head>
<?php
}

function SPITMENU($area) {
    $areas = array("people","publications","projects","contact");
    $areapages = array(
        "people" => "people",
        "contact" => "contact",
        "publications" => "pubs",
        "projects" => "projects"
    )
?>
<div id="menu-stack">
::
<a href="index">front page</a>
&rsaquo;&rsaquo;
<?php echo "<a href=\"$areapages[$area]\">$area</a>"; ?>
</div>

<div id="navigation">
view
<?php

$areastrings = array();
foreach ($areas as $a) {
    if ($a == $area) {
        continue;
    }
    array_push($areastrings,"<a href=\"$areapages[$a]\">$a</a>");
}

echo join(" :: ",$areastrings);

#<a href="people.php">people</a>
#::
#<a href="contact.php">contact</a>
?>
</div>

<div class="clear"></div>

<?php
}

?>
