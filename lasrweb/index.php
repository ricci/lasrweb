<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems eesearch (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include 'header.inc';
include 'tools.inc';

SPITHEADER("Flux Research Group - School of Computing, University of Utah","",TRUE);

?>

<body>
<div id="frontpage" class="page-container">

<div id="title">
<img src="pics/title.png" style="width:100%" alt="Flux Research Group / School of Computing">
</div>

<div id="main-picture" class="box-shadow">
<img src="pics/campus-fall.jpg" alt="University of Utah campus in fall">

<div id="front-menu"> 
<ul>
<li><a href="people" class="option">People</a></li>
<li><a href="pubs" class="option">Publications</a></li>
<li><a href="projects" class="option">Projects</a></li>
<li><a href="contact" class="option">Contact</a></li>
</ul>
</div>

<div id="about-us-box">
  <!-- TRANSPARENT BACKGROUND -->
  <div id="about-us-box-background" class="opaque-75"></div>
  <div id="about-us-box-content">
  <p class="section-title">
  <span class=light>about us</span>
  </p>

  <p>
  The Flux Research Group conducts research in <a href="area/os">operating systems</a>, <a href="area/net">networking</a>, 
  <a href="area/security">security</a>, and <a href="area/virt">virtualization</a>.
  </p>

  <p>Our group <a href="people">consists of</a> three
  faculty and over two dozen research staff, graduate students, and undergrads.
  We are part of the <a href="http://www.cs.utah.edu/">School of Computing</a>
  at the <a href="http://www.utah.edu/">University of Utah</a>.
  </p>
  </div>
</div>

<?php 
$news = file_get_contents('news.htm');
if ($news) {
  $news = trim($news);
  if (strlen($news) > 0) {
?>
<div id="news-box">
  <!-- TRANSPARENT BACKGROUND -->
  <div id="news-box-background" class="opaque-75"></div>

  <div id="news-box-content">
  <p class="section-title">
    <span class="light">recent news</span>
  </p>
  <?php
      echo $news;
  ?>
  </div>
</div>

<div id="oldsite">
<a href="http://www.cs.utah.edu/flux/oldsite.html">looking for something from our old site?</a>
</div>
<?php
  }
}
?>

</div> <!-- main-picture -->

<table id="frontpagetable" cellspacing=0 cellpadding=0 border=0>
<tr>

<td id="project-box" class="box-shadow projects-listing" rowspan=3>
<p class="section-title">
highlighted projects
<span class="subtitle"><a href="projects">(see more)</a></span>
</p>

<?php showProjectProfile("a3"); showProjectProfile("tcloud"); showProjectProfile("xcap"); showProjectProfile("emulab"); ?>

</td>

<td style="width:10px;padding:0px;" rowspan=3>
</td>

<td id="areas-box" class="box-shadow areas-listing">
<p class="section-title">research areas</p>
<div class="transparent-box">
<ul>
<?php
$result = mysql_query("SELECT RA.uid, RA.Name FROM ResearchArea AS RA LEFT JOIN RelPaperArea AS REL on RA.uid = REL.AID GROUP BY RA.uid ORDER BY COUNT(*) DESC");
while ($row = mysql_fetch_assoc($result)) {
    echo "<li><a href=\"area/" . tag("ResearchArea",$row['uid']) . "\">$row[Name]</a></li>\n";
}
?>
</ul>
<div class="clear"></div>
</div>
</td>

<tr>
<td style="height:10px;padding:0px;">
</td>
</tr>


<tr>
<td id="publications-box" class="box-shadow papers-listing">
<p class="section-title">
recent publications
<span class="subtitle"><a href="pubs">(see more)</a></span>
</p>

<?php getPublicationsModule(array("limit" => 7, "excludeTheses" => 1, "excludeTRs" => 1)); ?>
</td>
</tr>

</table>

<!-- LASR footer -->
<div id="footer">
<span style="float:left">Campus image courtesy of the University of Utah</span>
This site built using software originally developed by <a href="http://www.cs.utexas.edu/lasr" id="lasr">LASR</a>.
</div>

</div>

</body>
</html>

