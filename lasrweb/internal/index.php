<?php
include 'header.inc';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<html>
<head>
<title>Laboratory for Advanced Systems Research - Dept. of Computer Science, UT Austin</title>
<link href="../global.css" rel="stylesheet" type="text/css">
<link href="internal-frontpage.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="internal-front-page" class="page-container">

<div id="title">
<a href=".."><img src="../pics/title-internal.png" width="100%"></a>
</div>

<table cellpadding=0 cellspacing=0>
<tr>
<td id="add-info-box" class="box-shadow papers-listing">
<p class="section-title">add a new publication</p>

<p>
Congratulations!  Before adding your publication, please check that all 
authors (who are a member of the group, or an alum) are <a href="show_users.php">listed</a>. 
If not, <a href="user.php" class="primary-action">add an author</a>.
</p>

Click here to <a href="paper.php" class="primary-action">add a new publication</a>.
</p>

</td>
<td id="edit-info-box" class="box-shadow people-listing">
<p class="section-title">maintenance</p>

<p>
<a href="show_pubs.php" class="primary-action">See all publications</a>.
Use this to edit a specific publication.
</p>

<p>
<a href="show_users.php" class="primary-action">See all users</a>.
Use this to edit or delete a user.
</p>

<p>
<a href="show_projects.php" class="primary-action">Show all projects</a>.
Use this to create or edit project pages.
</p>
</td>
</tr>

</table>
</div>

</div>

</body>
</html>

