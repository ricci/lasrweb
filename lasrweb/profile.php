<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include 'header.inc';
include 'tools.inc';

$id = getIDFromParams("Person");

$result = mysql_query("SELECT * from Person where uid=$id;");
if(!$result || empty($result)) {
	die("Invalid unique id");
} 

$person = mysql_fetch_assoc($result);

if(empty($person)) {
	die("Invalid unique id");
}

SPITHEADER("$person[Full_name]");

$advisorID = $person["Advisor"];
$advisorID2 = $person["Advisor2"];
$advisorres1 = mysql_query("SELECT uid, Full_name FROM Person where uid=$advisorID;");
$advisorres2 = mysql_query("SELECT uid, Full_name FROM Person where uid=$advisorID2;");
if($advisorres1 && mysql_num_rows($advisorres1) > 0) {
	$advisor1 = mysql_fetch_assoc($advisorres1);
} else {
	$advisor1 = NULL;
}
if($advisorres2 && mysql_num_rows($advisorres2) > 0) {
	$advisor2 = mysql_fetch_assoc($advisorres2);
} else {
	$advisor2 = NULL;
}

#
# Find advisees
#
$advisee_res = mysql_query("SELECT uid, Full_name, Title FROM Person WHERE Advisor=$id or Advisor2=$id");

$advisees = array();
$alumni = array();

while($row = mysql_fetch_assoc($advisee_res)) {
    $userstring = "<a href=\"profile/" . tag("Person",$row['uid']) . "\">$row[Full_name]</a>";
    if ($row['Title'] == "alum") {
        array_push($alumni,$userstring);
    } else {
        array_push($advisees,$userstring);
    }
}


if($person["Picture"] == null) {
	$src = "pics/nobody.jpg";
} else {
	$src = "download?uid=".$person["uid"]."&amp;picture=1";
}

$areas = mysql_query("SELECT DISTINCT Name,A.uid FROM RelPersonArea as R,Person as P,ResearchArea as A WHERE A.uid=R.AreaID AND P.uid=R.PersonID AND P.uid=$id");
?>

<body>
<div id="profile-page" class="page-container">

<div id="title">
<a href="index"><img src="pics/title.png" style="width:100%" alt="Flux Research Group / School of Computing"></a>
</div>

<?php SPITMENU("people"); ?>

<?php if (personHasProjects($id)) { ?>

<div id="projects-section" class="box-shadow projects-listing">
<p class="section-title">projects</p>

<?php
$result = mysql_query("SELECT ProjectID FROM RelProjectPerson WHERE PersonID='$id'");
while ($row = mysql_fetch_assoc($result)) {
    showProjectProfile($row['ProjectID'],TRUE);
}
?>

</div>

<?php } ?>

<div id="leftside">

<div id="person-section" class="box-shadow people-listing">

<div class="transparent-box">

<div id="picture-cell">
<img src="<?php echo $src; ?>" alt="<?php echo $person["Full_name"]; ?>">
</div>

<div id=metadata>

<p class="section-title">
<?php echo $person["Full_name"]; ?>
</p>

<?php 
if(!is_null($advisor1)) {
  if ($person["Title"] == "postdoc") {
?>
<span id="position"><?php echo getPosition($person["Title"]);?></span>
<span id="advisor">with
<?php
  }
  else {
?>
<span id="position"><?php echo getPosition($person["Title"]);?></span>,
<span id="advisor">advised by 
<?php
  }
?>
  <a href="<?php echo "profile/" . tag("Person",$advisor1["uid"]);?>"><?php echo $advisor1["Full_name"];?></a>
  <?php if(!is_null($advisor2)) { ?>  
  and <a href="<?php echo "profile/" . tag("Person",$advisor2["uid"]);?>"><?php echo $advisor2["Full_name"];?></a>
<?php
  }
  ?>
  </span>
<?php
}
else {
?>
  <span id="position"><?php echo getPosition($person["Title"]);?></span>
<?php
}

if ($person["Title"] == "alum" && $person["WhereNow"]) {
    echo "<table id=\"aluminfo\" class=\"persondetails\"><tr><td><span class=\"small-header\">now at</span></td><td>$person[WhereNow]</td></tr></table>\n";
}


# Leave these off faculty pages
if (hasThesis($id) && $person["Title"] != "faculty" && $person["Title"] != "affilfaculty") {
    echo "<table id=\"theses\" class=\"persondetails\">\n";
    showPhDDiss($id);
    showMSThesis($id);
    echo "</table>\n";
}

?>


<?php
$area_count = mysql_num_rows($areas);
if ($areas && $area_count != 0) {
  echo "<table id=\"areas\" class=\"persondetails\">\n";
  echo "<tr><td>";
  if ($area_count > 1)
  {
?>
  <span class="small-header">areas of interest</span>
  <?php
  }
  else
  {
?>
  <span class="small-header">area of interest</span>
  <?php
  }
  echo "</td>\n";
  echo "<td>";
  $cnt = 1;
  while($row = mysql_fetch_assoc($areas)) {
    if($cnt < $area_count) {
      echo "<a href=\"area/".tag("ResearchArea",$row["uid"])."\">".strtolower($row['Name'])."</a>,\n";
    } else {
      echo "<a href=\"area/".tag("ResearchArea",$row["uid"])."\">".strtolower($row['Name'])."</a>\n";
    }
    $cnt++;
  }
  ?>
  </td></tr></table>
<?php
}
?>

<table id="contact" class="persondetails">

<?php if($person["Email"]) { ?>
<tr>
    <td>
        <span class="small-header">email</span>
    </td>
    <td>
        <?php echo str_replace("@"," at ",$person["Email"]);?>
    </td>
</tr>

<?php
} 
if($person["WWW"]) {
  $url = explode("//", $person["WWW"]);
  $url = $url[count($url)-1];
  if ($url[strlen($url)-1] == "/") {
    $url = substr($url, 0, strlen($url)-1);
  }
?>
<tr>
    <td>
        <span class="small-header">web</span>
    </td>
    <td>
        <a href="<?php echo $person["WWW"];?>"><?php echo $url;?></a><br>
    </td>
</tr>
<?php
}

if($person["Address"]) {
?>
<tr>
    <td>
        <span class="small-header">office</span>
    </td>
    <td>
        <?php echo $person["Address"];?>
    </td>
</tr>
<?php } ?>
</table>

<?php

    if (count($advisees) + count($alumni)) {
        echo "<table id=\"advisees\" class=\"persondetails\">\n";
        if (count($advisees)){
            echo "<tr><td><span class=\"small-header\">advisees</span></td> <td>" . join(", ",$advisees) . "</td></tr>\n";
        } 
        if (count($alumni)){
            echo "<tr><td><span class=\"small-header\">alumni</span></td> <td>" . join(", ",$alumni) . "</td></tr>\n";
        } 
        echo "</table>\n";
    }

?>


<div id="note">
  <?php echo $person["Note"];?>
</div>

</div>

<div class="clear"></div>

</div>

</div>

<?php if (personHasPapers($id)) { ?>

<div id="publication-section" class="box-shadow papers-listing">
<p class="section-title">publications with the flux group
</p>
<?php
$text = getPublications($id);
echo $text;
?>
</div>

<?php } ?>

</div>
</div>

</body>
</html>


