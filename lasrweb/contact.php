<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Copyright (c) 2012, Laboratory of Advanced Systems Research (LASR), Department
of Computer Science, The University of Texas at Austin

All rights reserved.

Redistribution and use of this code, with or without modification, are
permitted provided that the following conditions are met:

Redistributions must retain the above copyright notice, this list of
conditions, the footer labeled "LASR footer" at the bottom of the main page
(/index.php), and the following disclaimer.

Neither the name of LASR nor the names of its contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->

<?php
include 'header.inc';
include 'tools.inc';

SPITHEADER("Contact Us");

?>

<body>

<div id="contact-page" class="page-container">

<div id="title">
<a href="index"><img src="pics/title.png" width="100%" alt="Flux Research Group / School of Computing"></a>
</div>

<?php SPITMENU("contact"); ?>

<table cellpadding=0 cellspacing=0 style="clear:both">
<tr>
<td id="mailing-box" class="box-shadow">
<p class="section-title">mailing address</p>
<div class="transparent-box">
(name of recipient)<br>
University of Utah<br>
School of Computing<br>
50 S. Central Campus Dr., Rm.&nbsp;3190<br>
Salt Lake City, UT 84112&ndash;9205<br>
USA
</p>
</div>
</td>

<td style="width:10px;padding:0px"></td>

<td id="directions-box" class="box-shadow" rowspan=5>
<p class="section-title">directions</p>

<div class="transparent-box">
<p>
Our offices are in the <a 
href="http://www.map.utah.edu/?&xmin=428009.4&ymin=4513043.1&xmax=429170.3&ymax=4513781.3&find=64&aerial=off">Merrill 
Engineering Building</a> (<a href="http://goo.gl/maps/lp1cs">50 South Central Campus Drive</a>, Salt Lake City, UT), in the northeast corner of the third floor.
</p>

<p>If driving, you can park in the large lot to the north; the side of the lot closest to the building has a row of parking
meters. Enter via the northeast (uphill) door, go up one flight of stairs, and our offices will be immediately to your left.
</p>

<p>If you are visiting from out of town, the <a 
href="http://www.universityguesthouse.com/">University Guest House</a> is a 
good choice. Catch a <a 
href="http://www.parking.utah.edu/transportation/shuttles/index.html">free 
campus shuttle</a> on the Blue line from the street in front of the Guest House. The driver will be able to tell you
where the stop for MEB is. After getting off the bus, follow the instructions 
above. Alternately, the <a href="http://goo.gl/maps/uFR22">walk is quite pleasant</a> and takes about 25 minutes.
</p>

<p>From elsewhere in Salt Lake City, we are about a <a href="http://goo.gl/maps/z03G9">20 minute walk</a> from the Stadium stop on
the <a href="http://www.rideuta.com/mc/?page=uta-home-trax">TRAX light-rail red line</a>.</p>

<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=50+Central+Campus+Dr,+Salt+Lake+City,+UT&amp;aq=0&amp;oq=50+S&amp;sll=40.769366,-111.84626&amp;sspn=0.011473,0.022638&amp;ie=UTF8&amp;hq=&amp;hnear=50+Central+Campus+Dr,+Salt+Lake+City,+Salt+Lake,+Utah+84112&amp;t=m&amp;ll=40.769362,-111.846228&amp;spn=0.031202,0.054932&amp;z=14&amp;output=embed"></iframe><br>
</div>
</td>

<tr style="height:10px">
</tr>

<tr>
<td id="contact-box" class="box-shadow people-listing">

<p class="section-title">co-directors</p>

<?php showPersonProfile("ricci"); echo "<br>"; showPersonProfile("eeide"); echo "<br>"; showPersonProfile("kobus"); ?>

</td>
</tr>

</table>
</div>

</div>

</body>
</html>

